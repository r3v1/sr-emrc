\documentclass[a4paper]{article}

\usepackage{nips13submit_e, times}
\nipsfinaltrue

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subfig}
\usepackage{biblatex}

\usepackage[utf8]{inputenc}
\usepackage[spanish,es-tabla]{babel}

\usepackage{csquotes}
\usepackage{booktabs}

\title{Estructuras modulares en redes complejas \\ \small Semana 3}
\author{David Revillas \\ \texttt{drevillas002@ikasle.ehu.eus}}

\bibliography{bib}

\begin{document}
\maketitle

\begin{abstract}
    En el presente trabajo se implementan 2 ejemplos sistemas de recomendación: basado en contenido y filtro colaborativo, haciendo uso de \emph{The MovieLens Dataset}.
\end{abstract}

\section{Introducción}

En el proyecto que se presenta a continuación se estudian e implementan dos ejemplos de sistemas de recomendación, uno de ellos basado en contenido, y el otro, un filtro colaborativo. Para ello, se ha utilizado el conjunto de datos de películas \emph{MovieLens Dataset}\footnote{\url{https://grouplens.org/datasets/movielens/latest/}}. Se trata de una versión reducidad con información de 45000 películas, palabra clave, créditos, equipo profesional utilizado y valoraciones de usuarios para cada una de ellas.

\section{Sistema de recomendación basado en contenido}

Se trata de un sistema que recomienda ítems similares a los previamente valorados por el usuario. Éstos ítems se definen según unas características (e.g. palabras del documento) y se utiliza el espacio vectorial para representar usuarios y documentos.

En cuanto a sus ventajas, es capaz de recomendar según el contenido y no respecto a opiniones subjetivas de otros usuarios, se puede generalizar sobre la recomendación en sí basándose en el propio historial del usuario. Además, no hay dispersión puesto que la información se modela mediante las características del ítem y no de otros usuarios. Sin embargo, en cuanto a sus desventajas, se limita a recomendar contenido ya recomendado y un usuario nuevo, debería valorar un número concreto de ítems para que el sistema entienda sus preferencias. Es difícil para el sistema aprender a adaptarse, a los cambios en el perfil del usuario hasta no haber recolectado un número suficiente de valoraciones actualizadas.

En el caso de este tipo de sistema, las características utilizadas han sido la \emph{metadata} propia de cada película, que consiste en generar un registro con información de género cinematográficos, director principal, los cinco actores o actrices principales y una lista de palabras clave.

Una vez normalizadas dichas palabras, es decir, haber suprimido los espacios y puesto en minúsculas, se aplicará la similaridad del coseno para devolver las películas más semejantes entre sí:

\begin{equation*}
    \cos (\overrightarrow{i}, \overrightarrow{j}) = \frac{\overrightarrow{i} \cdot \overrightarrow{j}}{|\overrightarrow{i}|\cdot |\overrightarrow{j}|}
\end{equation*}

También se realiza un conteo de las palabras clave de la base de datos y se procesan de tal manera que sólo se quede la raíz de cada una de ellas para evitar confusiones.

De esta manera, la película \emph{Toy Story} se caracterizaría por las siguientes palabras:

\emph{jealousi, toy, boy, friendship, friend, rivalri, boynextdoor, newtoy, toycomestolif, tomhanks, timallen, donrickles, jimvarney, wallaceshawn, johnlasseter, animation, comedy, family}

Finalmente, una vez calculadas las similaridades, podemos calcular las recomendaciones para \emph{Shrek 2} en la Tabla \ref{tab:shrek2} o para \emph{Interstellar} en la Tabla \ref{tab:interstellar}, por ejemplo. Como se observa para éste segundo ejemplo, las películas recomendadas son también dirigidas por el mismo director.

\begin{table}[!htpb]
    \centering
    \begin{tabular}{lc}
        \toprule
        Película & Similaridad \\
        \midrule
        Shrek & 0.741 \\
        The Chronicles of Narnia: The Lion, the Witch... & 0.570 \\
        The Chronicles of Narnia: Prince Caspian & 0.554 \\
        Shrek the Third & 0.192 \\
        Shrek the Halls & 0.163 \\
        Shrek Forever After & 0.161 \\
        \bottomrule
    \end{tabular}
    \caption{\label{tab:shrek2}Recomendaciones basadas en \emph{Shrek 2}.}
\end{table}


\begin{table}[!htpb]
    \centering
    \begin{tabular}{lc}
        \toprule
        Película & Similaridad \\
        \midrule
        Inception & 0.472 \\
        The Prestige & 0.451 \\
        Following & 0.446 \\
        Memento & 0.425 \\
        Insomnia & 0.425 \\
        \bottomrule
    \end{tabular}
    \caption{\label{tab:interstellar}Recomendaciones basadas en \emph{Interstellar}.}
\end{table}

\section{Sistema de recomendación mediante filtro colaborativo}

Sin embargo, el sistema anterior sufre de algunas limitaciones: sólo sugiere ítems que son similares entre sí, es decir, no es capaz de capturar los gustos propios de los usuarios. Para solventar esta situación, se implementa el sistema de recomendación mediante filtro colaborativo, basado en la idea de que usuarios similares pueden predecir los gustos de un ítem concreto. Se realiza un seguimiento de usuarios similares, sugiriendo nuevos elementos basado en experiencias anteriores del mismo usuario y en opiniones de otros usuarios con mismos intereses.

Entre sus ventajas, se encuentra que en este proceso permite recomendar contenidos difíciles de analizar, válidas pero a veces no esperadas, lo que puede ser de utilidad. Al contrario, existen problemas para este método, como el problema de un usuario o ítem nuevo (\emph{cold-start}), el problema de la dispersión en el que si el número de usuarios es pequeño en relación al sistema se puede correr el riesgo de que el cubrimiento de valoraciones se vuelva muy disperso, o eun problema de escalabilidad, en el que a mayor número de usuarios implicará más computo, entre otros.

En este proceso, se tienen en cuenta:

\begin{itemize}
    \item Una matriz de valoraciones, en la que usuarios han valorado los ítems de la base de datos.
    \item Algoritmo de filtrado colaborativo, el cuál se entrena para predecir nuevas recomendaciones a partir de la matriz de valoraciones.
\end{itemize}


Para implementar este ejemplo, se ha utilizado la librería Surprise\cite{Hug2020} en Python. Se trata de una librería para generar y analizar sistemas de recomendación. Contiene varias implementaciones de algoritmos de predicción y se integra con la famosa librería Scikit-learn.

Se ha utilizado el algoritmo \emph{Singular Value Decomposition} (SVD) para realizar la factorización. Para evaluar su eficacia, se ha realizado una validación cruzada de 5 hojas y se observa que el \emph{Root Mean Squeare Error} (RMSE) se sitúa alrededor de 0.89, un error significativo teniendo en cuenta que el rango de valoraciones se sitúa entre 1 y 5.

Para probar el sistema, he añadido 28 valoraciones de películas y las he añadido a las casi 100000 valoraciones existentes de la base de datos. He entrenado el método de SVD y las películas recomendadas para mi usuario han sido los títulas de la Tabla \ref{tab:svd}.

\begin{table}[!htpb]
    \centering
    \begin{tabular}{lc}
        \toprule
        Película & Valoración predicha \\
        \midrule
        Quest & 4.636 \\
        The Million Dollar Hotel & 4.546 \\
        The 39 Steps & 4.475 \\
        Beverly Hills Cop III & 4.466 \\
        \bottomrule
    \end{tabular}
    \caption{\label{tab:svd}Filmes recomendados.}
\end{table}

Desconozco los títulos recomendados y después de haber visto los tráileres, por curiosidad, se alejan bastante de mis gustos reales. Convendría ajustar mejor el modelo.

\section{Mejoras}

Como mejora del sistema de recomendación, se podría considerar la hibidridación de ambos sistemas, es decir, explotar las características de ambos sistemas para intentar solventar las desventajas de los dos.


\section{Material suplementario}

Es posible acceder al repositorio \url{https://gitlab.com/r3v1/sr-emrc} para acceder tanto a la base de datos utilizada como al código utilizado.

\printbibliography
\end{document}
